i3-gaps configuration for use with the XFCE4 DE running i3-gaps instead of XFWM

>

install nitrogen, as the XFCE4 wallpaper manager will not work with i3

>

install i3-gaps

>

open session & startup, and go to the startup tab.

For xfwm4, click 'Immediately' and change it to the  'Never' option.

For xfdesktop, click 'Immediately' and change it to the 'Never' option.

Click the button: 'Save Session'.

Go to the 'Application Autostart' tab to activate the i3 window manager in the next stage.

In the 'Session and Startup' window, make sure you are in the 'Application Autostart' tab.

Click the button 'Add' to add i3 to the list of startup applications.

Fill out the form:

Name: i3-gaps (or whatever you want to call i3)
Description: *whatever you want it to be*
Command: i3-gaps

click OK

scroll to the bottom and confirm i3-gaps is listed and checked

click close

>

Open the 'Keyboard' dialogue.

disable all stock XFCE4 keyboard shortcuts

>

paste i3 config.

> 

and you're now ready to have a clean i3-gaps + XFCE4 config.

>

credit to FeebleNerd for i3 XFCE4 setup instructions.

